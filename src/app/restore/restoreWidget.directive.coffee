linkFnct = ($scope, element, attrs) ->

restoreWidgetFunction = ->
  obj = {
    restrict: 'E',
    link: linkFnct,
    controller: 'RestoreWidgetController',
    controllerAs: 'RestoreWidgetCtrl',
    scope: {
    },
    templateUrl: './app/restore/restoreWidget.html'
  }
  return obj

angular.module 'calendar'
  .directive 'restoreWidget', restoreWidgetFunction
