RestoreWidgetController = (CalendarService,ngDialog) ->
  'ngInject'

  getFile = ($fileContent) ->
    try
      vm.data = JSON.parse($fileContent)
    catch error
      console.log "Error on getFile on restoreWidget: " + JSON.stringify error
      return

    return

  _rollback = () ->
    CalendarService.setCalendars(vm.oldCalendars)
    CalendarService.setShifts(vm.oldShifts)
    CalendarService.setEvents(vm.oldEvents)
    CalendarService.setShiftEvents(vm.oldShiftEvents)
    console.log 'Rolling back backup'
    return

  restore = () ->
    console.log "Data to be restored :  " + JSON.stringify vm.data
    vm.oldCalendars = CalendarService.getCalendars()
    vm.oldEvents = CalendarService.getEvents()
    vm.oldShifts = CalendarService.getShifts()
    vm.oldShiftEvents = CalendarService.getShiftEvents()
    CalendarService.clearStorage()
    CalendarService.setCalendars(vm.data.calendars)
    CalendarService.setEvents(vm.data.events)
    CalendarService.setShifts(vm.data.shifts)
    CalendarService.setShiftEvents(vm.data.shiftEvents)
#    CalendarService.refreshCalendar()
    return

  showDialog = (cad) ->
    ngDialog.open({
      template: cad,
      plain: true
    })

  showRestoreConfirm = () ->

    functionOK = () ->
      try
        restore()
        cad = '<p>Backup restaurado correctamente.</p>'
        vm.showDialog(cad)
        view='Calendario'
      catch error
        _rollback()
        cad = '<p>Error al restaurar backup.</p><br><p>Error: ' + JSON.stringify error
        cad += '</p>'
        vm.showDialog(cad)
      return

    functionCancel = () ->
      return

    dialogPromise = ngDialog.openConfirm({ template: './app/restore/restoreConfirmDialog.html', className: 'ngdialog-theme-default' })
    dialogPromise.then functionOK, functionCancel

  vm = this
  vm.getFile = getFile
  vm.restore = restore
  vm.showDialog = showDialog
  vm.showRestoreConfirm = showRestoreConfirm

  return

RestoreWidgetController.$inject = ['CalendarService','ngDialog']

angular.module 'calendar'
  .controller 'RestoreWidgetController', RestoreWidgetController

