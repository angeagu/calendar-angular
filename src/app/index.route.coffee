angular.module 'calendar'
  .config ($routeProvider) ->
    'ngInject'
    $routeProvider
      .when '/',
        templateUrl: './app/main/main.html'
        controller: 'CalendarWidgetController'
        controllerAs: 'ctrl'
      .otherwise
        redirectTo: '/'
