CalendarMenuController = (CalendarService,ngDialog,$scope) ->
  'ngInject'

  addCalendar = ->
    dialog = ngDialog.open({ template: './app/calendar-menu/addCalendarDialog.html', className: 'ngdialog-theme-default' })
    dialog.closePromise.then calendarDialogCloseHandler
    return

  addEvent = ->
    dialog = ngDialog.open({ template: './app/calendar-menu/addEventDialog.html', className: 'ngdialog-theme-default' })
    dialog.closePromise.then eventDialogCloseHandler
    return

  addShift = ->
    dialog = ngDialog.open({ template: './app/calendar-menu/addShiftDialog.html', scope:$scope, className: 'ngdialog-theme-default' })
    dialog.closePromise.then shiftDialogCloseHandler
    return

  calendarDialogCloseHandler = (response) ->
    CalendarService.addCalendar(response.value) if response.value isnt null and not response.value.startsWith '$'
    return

  exportImage = ->
    html2canvas document.querySelector(".divCalendarWidget")
    .then (canvas) ->
      a = document.createElement 'a'
      a.href = canvas.toDataURL 'image/jpeg'
      a.download = 'Test.jpg'
      a.click()

    return

  eventDialogCloseHandler = (response) ->
    CalendarService.addEvent(response.value) if response.value isnt null && typeof response.value is 'object'
    return

  shiftDialogCloseHandler = (response) ->
    CalendarService.addShift(response.value) if response.value isnt null && typeof response.value is 'object'
    return

  switchCalendar = () ->
    CalendarService.setCurrentCalendar vm.calendarSelected
    return

  switchShift = () ->
    CalendarService.setCurrentShift vm.shiftSelected
    return

  shiftColorOptions =
    swatchOnly: true,
    format: 'hex'

  vm = @
  vm.addCalendar = addCalendar
  vm.addEvent = addEvent
  vm.addShift = addShift
  vm.exportImage = exportImage
  vm.switchCalendar = switchCalendar
  vm.switchShift = switchShift
  vm.calendars = CalendarService.getCalendars()
  vm.shifts = CalendarService.getShifts()
  vm.calendarSelected = vm.calendars[0].id if vm.calendars.length > 0
  vm.shiftSelected = vm.shifts[0].id if vm.shifts.length > 0
  $scope.shiftColorOptions = shiftColorOptions

  return

CalendarMenuController.$inject = ['CalendarService','ngDialog','$scope','$timeout']

angular.module 'calendar'
  .controller 'CalendarMenuController', CalendarMenuController

