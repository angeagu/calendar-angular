linkFnct = ($scope, element, attrs) ->

calendarMenuFunction = ->
  obj = {
    restrict: 'E',
    link: linkFnct,
    controller:'CalendarMenuController',
    controllerAs:'CalendarMenuCtrl',
    scope: {
    },
    templateUrl:'./app/calendar-menu/calendarMenu.html'
  }
  return obj

angular.module 'calendar'
  .directive 'calendarMenu', calendarMenuFunction
