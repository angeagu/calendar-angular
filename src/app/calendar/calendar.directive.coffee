linkFnct = ($scope, element, attrs) ->
  divCalendarNode = angular.element element.find '.divCalendarWidget'
#  $scope.divCalendarNode = divCalendarNode
  return

calendarWidgetFunction = (CalendarService,_) ->

  obj = {
    restrict: 'E',
    link: linkFnct
    controller:'CalendarWidgetController',
    controllerAs:'CalendarWidgetCtrl',
    templateUrl:'./app/calendar/calendar.html'
  }
  return obj

calendarWidgetFunction.$inject = ['CalendarService','lodash']

angular.module 'calendar'
  .directive 'calendarWidget', calendarWidgetFunction


