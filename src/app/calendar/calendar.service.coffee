CalendarService = ($localStorage,_,ngDialog) ->

  _calendar = null
  _currentShift = 0
  _currentCalendar = 0

  addCalendar = (name) ->
    id = $localStorage.calendars.length + 1
    $localStorage.calendars.push({id: id, name: name})
    return id

  addEvent = (event) ->
    event.id = $localStorage.events.length + 1
    event.calendarId = getCurrentCalendar()
    $localStorage.events.push(event)
    _calendar.fullCalendar('renderEvent', event, true)
    return

  addEventsToCalendar = (calendarId,events) ->
    _.forEach events , (event) ->
      event.calendarId is calendarId
    _.concat getEvents(), events
    return

  addShift = (shift) ->
    id = $localStorage.shifts.length + 1
    shift.id = id
    shift.color = '#'+shift.color if not shift.color.startsWith '#'
    $localStorage.shifts.push(shift)
    return

  addShiftEventsToCalendar = (calendarId,shiftEvents) ->
    _.forEach shiftEvents , (shiftEvent) ->
      shiftEvent.calendarId is calendarId
    _.concat getShiftEvents(), shiftEvents
    return

  clearStorage = () ->
    $localStorage.calendars = []
    $localStorage.events = []
    $localStorage.shifts = []
    $localStorage.shiftEvents = []

  getCurrentCalendar = () ->
    return _currentCalendar

  getCurrentShift = () ->
    found = null
    _.each $localStorage.shifts, (item) ->
      if item.id is _currentShift
        found = item
    return found

  getCalendar = (calendarId) ->
    calendarFound = null
    _.forEach $localStorage.calendars, (calendar) ->
      if calendar.id is calendarId
        calendarFound = calendar
    return calendarFound

  getCalendars = () ->
    return $localStorage.calendars

  getCalendarEvents = (calendarId) ->
    events = []
    _.each $localStorage.events, (item) ->
      if item.calendarId is calendarId
        events.push item
      return
    return events

  getCalendarShiftEvents = (calendarId) ->
    shiftEvents = []
    _.each $localStorage.shiftEvents, (item) ->
      if item.calendarId is calendarId
        shiftEvents.push item
      return
    return shiftEvents

  getEvents = () ->
    return $localStorage.events

  getShift = (shiftId) ->
    shiftFound = null
    _.each $localStorage.shifts, (storedShift) ->
      if storedShift.id is shiftId
        shiftFound = storedShift
    return shiftFound

  getShifts = () ->
    return $localStorage.shifts

  getShiftEvents = () ->
    return $localStorage.shiftEvents

  isCalendarCreated = () ->
    return _calendar?

  refreshCalendar = () ->
    _calendar.fullCalendar 'removeEvents'
    _.each $localStorage.events, (event) ->
      if event.calendarId is _currentCalendar
        console.log 'rendering standard events'
        _calendar.fullCalendar('renderEvent', event, true)
        return
    _.each $localStorage.shiftEvents, (shiftEvent) ->
      if shiftEvent.calendarId is _currentCalendar
        console.log('Rendering shiftEvent: ' + JSON.stringify shiftEvent)
        _calendar.fullCalendar('renderEvent', shiftEvent, true)
        return
    return

  refreshShiftEvents = (shiftId) ->
    shiftEventsToRemove = []
    _.each $localStorage.shiftEvents, (shiftEvent) ->
        if shiftEvent.shiftId = shiftId
          shiftEventsToRemove.push shiftEvent.id
        return

    _calendar.fullCalendar 'removeEvents', shiftEventsToRemove
    console.log 'removedEvents ' + JSON.stringify shiftEventsToRemove
    _.each $localStorage.shiftEvents, (shiftEvent) ->
      if shiftEvent.shiftId is shiftId
        _calendar.fullCalendar('renderEvent', shiftEvent, true)
        console.log 'refreshing shiftEvent...'
        return
    return

  removeCalendar = (calendarId) ->
    _.remove getCalendars(), (calendar) ->
      return calendar.id is calendarId
    removeEventsFromCalendar(calendarId)
    removeShiftEventsFromCalendar(calendarId)
    return

  removeEventsFromCalendar = (calendarId) ->
    _.remove getEvents(), (event) ->
      return event.calendarId is calendarId

  removeShift = (shiftId) ->
    _.remove getShifts(), (shift) ->
      return shift.id is shiftId
    return

  removeShiftEventsByShiftId = (shiftId) ->
    _.remove getShiftEvents(), (shiftEvent) ->
      return shiftEvent.shiftId is shiftId

  removeShiftEventsFromCalendar = (calendarId) ->
    _.remove getShiftEvents(), (shiftEvent) ->
      return shiftEvent.calendarId is calendarId

  setCalendarInstance = (calendarInstance) ->
    _calendar = calendarInstance
    return

  setCalendars = (calendars) ->
    $localStorage.calendars = calendars
    return

  setCurrentCalendar = (calendarId) ->
    _currentCalendar = calendarId
    refreshCalendar() if _calendar isnt null
    return

  setCurrentShift = (shiftId) ->
    _currentShift = shiftId

  setEvents = (events) ->
    $localStorage.events = events
    return

  setShifts = (shifts) ->
    $localStorage.shifts = shifts
    return

  setShiftEvents = (shiftEvents) ->
    $localStorage.shiftEvents = shiftEvents
    return

  if !$localStorage.calendars
    $localStorage.calendars = []
  if !$localStorage.events
    $localStorage.events = []
  if !$localStorage.shifts
    $localStorage.shifts = []
  if !$localStorage.shiftEvents
    $localStorage.shiftEvents = []

  #
  # The API
  #
  factory =
    addCalendar: addCalendar
    addEvent: addEvent
    addEventsToCalendar: addEventsToCalendar
    addShift: addShift
    addShiftEventsToCalendar: addShiftEventsToCalendar
    clearStorage: clearStorage
    isCalendarCreated: isCalendarCreated
    getCalendar: getCalendar
    getCalendars: getCalendars
    getCalendarEvents: getCalendarEvents
    getCalendarShiftEvents: getCalendarShiftEvents
    getCurrentCalendar: getCurrentCalendar
    getCurrentShift: getCurrentShift
    getEvents: getEvents
    getShift: getShift
    getShifts: getShifts
    getShiftEvents: getShiftEvents
    refreshCalendar: refreshCalendar
    refreshShiftEvents: refreshShiftEvents
    removeCalendar: removeCalendar
    removeEventsFromCalendar: removeEventsFromCalendar
    removeShift: removeShift
    removeShiftEventsByShiftId: removeShiftEventsByShiftId
    removeShiftEventsFromCalendar: removeShiftEventsFromCalendar
    setCalendarInstance: setCalendarInstance
    setCalendars: setCalendars
    setCurrentCalendar: setCurrentCalendar
    setCurrentShift: setCurrentShift
    setShifts: setShifts
    setShiftEvents: setShiftEvents
    setEvents: setEvents

  return factory

CalendarService.$inject = ['$localStorage','lodash','ngDialog']

angular.module "calendar"
  .factory "CalendarService",CalendarService
