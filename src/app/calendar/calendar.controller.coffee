CalendarWidgetController = (CalendarService,$localStorage,$scope,ngDialog) ->
  'ngInject'

  vm = this

  _dayClickHandler = (date, jsEvent, view) ->
    if $(this).css('backgroundColor')
      $(this).removeClass('backgroundColor')

    if view.type is 'month'
      shiftEvent =
        id: new Date().getTime()
        shiftId: CalendarService.getCurrentShift().id
        calendarId: CalendarService.getCurrentCalendar()
        start: date
        allDay: true
        rendering: 'background'
        editable: true
        backgroundColor: 'white'
      #TODO THIS IS SUPER UGLY!!
      vm.calendarInstance.fullCalendar('renderEvent', shiftEvent, true)
      vm.calendarInstance.fullCalendar('renderEvent', shiftEvent, true)
      vm.calendarInstance.fullCalendar('renderEvent', shiftEvent, true)
      vm.calendarInstance.fullCalendar('renderEvent', shiftEvent, true)
      vm.calendarInstance.fullCalendar('renderEvent', shiftEvent, true)
      vm.calendarInstance.fullCalendar('renderEvent', shiftEvent, true)
      vm.calendarInstance.fullCalendar('renderEvent', shiftEvent, true)
      vm.calendarInstance.fullCalendar('renderEvent', shiftEvent, true)
      shiftEvent.backgroundColor = CalendarService.getCurrentShift().color
      vm.calendarInstance.fullCalendar('renderEvent', shiftEvent, true)
      console.log 'Shift Event Added: ' + JSON.stringify shiftEvent
      $localStorage.shiftEvents.push(shiftEvent)

    return

  _dayRenderHandler = (date, element, view) ->
    console.log 'Day render handler'
    #TODO This doesn't work
    element.bind "dblclick", ->
      alert "double click!"
    return

  _eventClickHandler = (calEvent, jsEvent, view) ->
    $scope.editEvent = calEvent
    dialog = ngDialog.open
      template: './app/calendar/editEventDialog.html'
      className: 'ngdialog-theme-default'
      scope: $scope

    dialog.closePromise.then (response) ->
      #CalendarService.addEvent(response.value) if response.value isnt null && typeof response.value is 'object'
      #TODO handle values from edited event.
    return

  createCalendar = (node) ->
    _calendar = node
    _calendar.fullCalendar(
      aspectRatio: 1,
      header:
        left: 'title'
        center: 'month,agendaWeek,agendaDay,listYear'
        right: 'prev,next,today'
      views:
        listYear:
          type: 'list',
          duration: { days: 365},
          buttonText: 'Año'
      titleFormat: 'MMMM YYYY'
      locale: 'es',
      navLinks: true,
      editable: true,
      eventLimit: true,
      eventClick: _eventClickHandler,
      dayClick: _dayClickHandler,
      dayRender: _dayRenderHandler,
      events: _.union(CalendarService.getCalendarEvents(),CalendarService.getShiftEvents())
    )

    return _calendar

  initCalendar = ->
    divCalendarNode = angular.element angular.element.find '.divCalendarWidget'
    vm.calendarInstance = createCalendar(divCalendarNode)
    CalendarService.setCalendarInstance(vm.calendarInstance)
    CalendarService.addCalendar('Calendario1') if CalendarService.getCalendars().length is 0
    CalendarService.setCurrentCalendar(CalendarService.getCalendars()[0].id)
    return

  initCalendar()

  return

CalendarWidgetController.$inject = ['CalendarService','$localStorage','$scope','ngDialog']

angular.module 'calendar'
  .controller 'CalendarWidgetController', CalendarWidgetController
