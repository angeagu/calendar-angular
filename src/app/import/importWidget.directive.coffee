linkFnct = ($scope, element, attrs) ->

importWidgetFunction = ->
  obj = {
    restrict: 'E',
    link: linkFnct,
    controller: 'ImportWidgetController',
    controllerAs: 'ImportWidgetCtrl',
    scope: {
    },
    templateUrl:'./app/import/importWidget.html'
  }
  return obj

angular.module 'calendar'
  .directive 'importWidget', importWidgetFunction
