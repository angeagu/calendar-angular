ImportWidgetController = (CalendarService,$scope,ngDialog) ->
  'ngInject'

  getFile = ($fileContent) ->
    try
      vm.data = JSON.parse($fileContent)
      console.log "Data readed from file: " + JSON.stringify vm.data
    catch error
      console.log "Error on getFile on importWidget: " + JSON.stringify error
      return

    return


  importFile =  ->
    calendars = CalendarService.getCalendars()
    _.forEach vm.data.calendars, (cal) ->
      calendarIdFound = -1
      _.forEach calendars, (calendar) ->
        if calendar.name is cal.name
          calendarIdFound = calendar.id
        return

      if calendarIdFound > -1
        $scope.calendarFoundName = cal.name
        showOverrideConfirm cal.name
        .then () ->
          CalendarService.removeEventsFromCalendar(calendarIdFound)
          CalendarService.removeShiftEventsFromCalendar(calendarIdFound)
          CalendarService.addEventsToCalendar(calendarIdFound,vm.data.events)
          CalendarService.addShiftEventsToCalendar(calendarIdFound,vm.data.shiftEvents)
          return
        ,
          () ->
            return
      else
        idCalendar = CalendarService.addCalendar cal.name
        CalendarService.addEventsToCalendar(idCalendar,vm.data.events)
        CalendarService.addShiftEventsToCalendar(idCalendar,vm.data.shiftEvents)
      return

    view='Calendario'
    return

  showDialog = (cad) ->
    ngDialog.open({
      template: cad,
      plain: true
    })

#  showImportConfirm = ->
#    functionOK = ->
#      try
#        importFile()
#        cad = '<p>Importacion de calendarios finalizada</p>'
#        vm.showDialog(cad)
#        view='Calendario'
#      catch error
#        cad = '<p>Error al importar calendarios.</p><br><p>Error: ' + JSON.stringify error
#        cad += '</p>'
#        vm.showDialog(cad)
#      return
#
#    functionCancel = () ->
#      return
#
#    dialogPromise = ngDialog.openConfirm({ template: './app/restore/importConfirmDialog.html', className: 'ngdialog-theme-default' })
#    dialogPromise.then functionOK, functionCancel

  showOverrideConfirm = (calendarName) ->
    dialogConfig =
      template: './app/import/importConfirmDialog.html'
      className: 'ngdialog-theme-default'
      scope: $scope
    dialogPromise = ngDialog.openConfirm dialogConfig
    return dialogPromise

  vm = this
  vm.getFile = getFile
  vm.importFile = importFile

  return

ImportWidgetController.$inject = ['CalendarService','$scope','ngDialog','lodash']

angular.module 'calendar'
  .controller 'ImportWidgetController', ImportWidgetController

