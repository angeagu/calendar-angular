filereadFunction = ($parse) ->

  linkFnct = (scope, element, attrs) ->

    fn = $parse attrs.fileread

    onFileLoadFunction = (loadEvent) ->
      applyFunction = () ->
        fn scope, {$fileContent:loadEvent.target.result}
        console.log 'fileread: ' + JSON.stringify {$fileContent:loadEvent.target.result}
        return

      scope.$apply applyFunction

      return

    changeFunction = (changeEvent) ->
      reader = new FileReader()
      reader.onload = onFileLoadFunction
      elem = changeEvent.srcElement || changeEvent.target
      reader.readAsText elem.files[0]
      return

    element.on "change",changeFunction

    return

  obj = {
    scope: false
    restrict: 'A'
    link: linkFnct
  }
  return obj

filereadFunction.$inject = ['$parse']

angular.module 'calendar'
  .directive 'fileread', filereadFunction
