linkFnct = ($scope, element, attrs) ->

gestionWidgetFunction = ->
  obj = {
    restrict: 'E',
    link: linkFnct,
    controller: 'GestionWidgetController',
    controllerAs: 'GestionWidgetCtrl',
    scope: {
    },
    templateUrl:'./app/gestion/gestionWidget.html'
  }
  return obj

angular.module 'calendar'
  .directive 'gestionWidget', gestionWidgetFunction
