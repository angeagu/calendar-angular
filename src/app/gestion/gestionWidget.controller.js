// Generated by CoffeeScript 1.12.5
var GestionWidgetController;

GestionWidgetController = function(CalendarService, ngDialog, _, $scope) {
  'ngInject';
  var calendarCellDelete, calendarCellModify, deleteCalendar, deleteShift, editCalendar, editShift, initGrids, shiftCellColor, shiftCellDelete, shiftCellModify, vm;
  vm = this;
  shiftCellColor = '<div class="ui-grid-cell-contents" style="background-color: {{grid.getCellValue(row, col)}}" ></div>';
  shiftCellDelete = '<div class="fa fa-trash-o" ng-click="grid.appScope.GestionWidgetCtrl.deleteShift(grid.getCellValue(row, grid.columns[0]))">&nbsp;&nbsp;</div>';
  shiftCellModify = '<div class="fa fa-pencil-square-o" ng-click="grid.appScope.GestionWidgetCtrl.editShift(grid.getCellValue(row, grid.columns[0]))">&nbsp;&nbsp;</div>';
  calendarCellModify = '<div class="fa fa-pencil-square-o" ng-click="grid.appScope.GestionWidgetCtrl.editCalendar(grid.getCellValue(row, grid.columns[0]))">&nbsp;&nbsp;</div>';
  calendarCellDelete = '<div class="fa fa-trash-o" ng-click="grid.appScope.GestionWidgetCtrl.deleteCalendar(grid.getCellValue(row, grid.columns[0]))">&nbsp;&nbsp;</div>';
  vm.calendarGridOptions = {
    enableFiltering: true,
    treeRowHeaderAlwaysVisible: true,
    columnDefs: [
      {
        field: 'id',
        displayName: 'ID'
      }, {
        field: 'name',
        displayName: 'Nombre'
      }, {
        name: 'opciones',
        displayName: 'Opciones',
        cellTemplate: '<div class="ui-grid-cell-contents" >' + calendarCellModify + calendarCellDelete + '</div>',
        enableFiltering: false,
        enableColumnMenu: false
      }
    ],
    onRegisterApi: function(gridApi) {
      return vm.gridApi = gridApi;
    }
  };
  vm.shiftGridOptions = {
    enableFiltering: true,
    treeRowHeaderAlwaysVisible: true,
    columnDefs: [
      {
        field: 'id',
        displayName: 'ID'
      }, {
        field: 'name',
        displayName: 'Nombre'
      }, {
        field: 'color',
        displayName: 'Color',
        cellTemplate: shiftCellColor
      }, {
        name: 'opciones',
        displayName: 'Opciones',
        cellTemplate: '<div class="ui-grid-cell-contents" >' + shiftCellModify + shiftCellDelete + '</div>',
        enableFiltering: false,
        enableColumnMenu: false
      }
    ],
    onRegisterApi: function(gridApi) {
      return vm.gridApi = gridApi;
    }
  };
  vm.managementGridOptions = {
    enableFiltering: true,
    treeRowHeaderAlwaysVisible: true,
    columnDefs: [
      {
        field: 'id',
        displayName: 'ID'
      }, {
        field: 'name',
        displayName: 'Nombre'
      }
    ],
    onRegisterApi: function(gridApi) {
      return vm.gridApi = gridApi;
    }
  };
  deleteCalendar = function(calendarId) {
    var calendar, dialog;
    $scope.deleteCalendarId = -1;
    calendar = CalendarService.getCalendar(calendarId);
    if (calendar != null) {
      $scope.deleteCalendarName = calendar.name;
      $scope.deleteCalendarId = calendar.id;
    }
    dialog = ngDialog.open({
      template: './app/gestion/deleteCalendarDialog.html',
      className: 'ngdialog-theme-default',
      scope: $scope
    });
    dialog.closePromise.then(function(response) {
      if (response.value && $scope.deleteCalendarId > -1) {
        CalendarService.removeShiftEventsFromCalendar(calendarId);
        CalendarService.removeEventsFromCalendar(calendarId);
        return CalendarService.removeCalendar(calendarId);
      }
    });
  };
  deleteShift = function(shiftId) {
    var dialog, shifts;
    $scope.deleteShiftId = -1;
    shifts = CalendarService.getShifts();
    _.forEach(shifts, function(shift) {
      if (shift.id === shiftId) {
        $scope.deleteShiftName = shift.name;
        $scope.deleteShiftId = shift.id;
      }
    });
    dialog = ngDialog.open({
      template: './app/gestion/deleteShiftDialog.html',
      className: 'ngdialog-theme-default',
      scope: $scope
    });
    dialog.closePromise.then(function(response) {
      if (response.value && $scope.deleteShiftId > -1) {
        CalendarService.removeShiftEventsByShiftId(shiftId);
        return CalendarService.removeShift(shiftId);
      }
    });
  };
  editCalendar = function(calendarId) {
    var calendar, dialog;
    $scope.editCalendarId = -1;
    calendar = CalendarService.getCalendar(calendarId);
    if (calendar != null) {
      $scope.editCalendarName = calendar.name;
      $scope.editCalendarId = calendar.id;
    }
    dialog = ngDialog.open({
      template: './app/gestion/editCalendarDialog.html',
      className: 'ngdialog-theme-default',
      scope: $scope
    });
    dialog.closePromise.then(function(response) {
      if (response.value && (!response.value.startsWith('$')) && $scope.editCalendarId > -1) {
        return calendar.name = response.value;
      }
    });
  };
  editShift = function(shiftId) {
    var dialog, shift;
    $scope.editShiftId = -1;
    shift = CalendarService.getShift(shiftId);
    if (shift != null) {
      $scope.editShiftName = shift.name;
      $scope.editShiftId = shift.id;
      $scope.editShiftColor = shift.color;
    }
    dialog = ngDialog.open({
      template: './app/gestion/editShiftDialog.html',
      className: 'ngdialog-theme-default',
      scope: $scope
    });
    dialog.closePromise.then(function(response) {
      var shiftEvents;
      if (response.value.editShiftName && $scope.editShiftId > -1) {
        shift.name = response.value.editShiftName;
        shift.color = response.value.editShiftColor;
        shiftEvents = CalendarService.getShiftEvents();
        return _.each(shiftEvents, function(shiftEvent) {
          if (shiftEvent.shiftId === shiftId) {
            return shiftEvent.backgroundColor = shift.color;
          }
        });
      }
    });
  };
  initGrids = function() {
    vm.calendarGridOptions.data = CalendarService.getCalendars();
    vm.shiftGridOptions.data = CalendarService.getShifts();
  };
  vm.editCalendar = editCalendar;
  vm.editShift = editShift;
  vm.deleteCalendar = deleteCalendar;
  vm.deleteShift = deleteShift;
  initGrids();
};

GestionWidgetController.$inject = ['CalendarService', 'ngDialog', 'lodash', '$scope'];

angular.module('calendar').controller('GestionWidgetController', GestionWidgetController);
