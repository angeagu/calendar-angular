GestionWidgetController = (CalendarService,ngDialog,_,$scope) ->
  'ngInject'

  vm = this

  shiftCellColor = '<div class="ui-grid-cell-contents" style="background-color: {{grid.getCellValue(row, col)}}" ></div>'
  shiftCellDelete= '<div class="fa fa-trash-o" ng-click="grid.appScope.GestionWidgetCtrl.deleteShift(grid.getCellValue(row, grid.columns[0]))">&nbsp;&nbsp;</div>'
  shiftCellModify= '<div class="fa fa-pencil-square-o" ng-click="grid.appScope.GestionWidgetCtrl.editShift(grid.getCellValue(row, grid.columns[0]))">&nbsp;&nbsp;</div>'
  calendarCellModify ='<div class="fa fa-pencil-square-o" ng-click="grid.appScope.GestionWidgetCtrl.editCalendar(grid.getCellValue(row, grid.columns[0]))">&nbsp;&nbsp;</div>'
  calendarCellDelete = '<div class="fa fa-trash-o" ng-click="grid.appScope.GestionWidgetCtrl.deleteCalendar(grid.getCellValue(row, grid.columns[0]))">&nbsp;&nbsp;</div>'

  vm.calendarGridOptions =
    enableFiltering: true
    treeRowHeaderAlwaysVisible: true
    columnDefs: [
      { field: 'id', displayName: 'ID'}
      { field: 'name', displayName: 'Nombre'}
      { name: 'opciones', displayName: 'Opciones', cellTemplate: '<div class="ui-grid-cell-contents" >'+calendarCellModify+calendarCellDelete+'</div>', enableFiltering:false, enableColumnMenu: false}
    ]
    onRegisterApi: ( gridApi ) ->
      vm.gridApi = gridApi


  vm.shiftGridOptions =
    enableFiltering: true
    treeRowHeaderAlwaysVisible: true
    columnDefs: [
      { field: 'id', displayName: 'ID'}
      { field: 'name', displayName: 'Nombre'}
      { field: 'color', displayName: 'Color', cellTemplate: shiftCellColor}
      { name: 'opciones', displayName: 'Opciones', cellTemplate: '<div class="ui-grid-cell-contents" >'+shiftCellModify+shiftCellDelete+'</div>',enableFiltering:false, enableColumnMenu: false}
    ]
    onRegisterApi: ( gridApi ) ->
      vm.gridApi = gridApi


  vm.managementGridOptions =
    enableFiltering: true
    treeRowHeaderAlwaysVisible: true
    columnDefs: [
      { field: 'id', displayName: 'ID'}
      { field: 'name', displayName: 'Nombre'}
    ]
    onRegisterApi: ( gridApi ) ->
      vm.gridApi = gridApi


  deleteCalendar = (calendarId) ->
    $scope.deleteCalendarId = -1
    calendar = CalendarService.getCalendar(calendarId)
    if calendar?
      $scope.deleteCalendarName = calendar.name
      $scope.deleteCalendarId = calendar.id

    dialog = ngDialog.open
      template: './app/gestion/deleteCalendarDialog.html',
      className: 'ngdialog-theme-default'
      scope: $scope
    dialog.closePromise
      .then (response) ->
        if response.value and $scope.deleteCalendarId > -1
          CalendarService.removeShiftEventsFromCalendar calendarId
          CalendarService.removeEventsFromCalendar calendarId
          CalendarService.removeCalendar calendarId
    return

  deleteShift = (shiftId) ->
    $scope.deleteShiftId = -1
    shifts = CalendarService.getShifts()
    _.forEach shifts, (shift) ->
      if shift.id is shiftId
        $scope.deleteShiftName = shift.name
        $scope.deleteShiftId =  shift.id
      return

    dialog = ngDialog.open
      template: './app/gestion/deleteShiftDialog.html',
      className: 'ngdialog-theme-default'
      scope: $scope

    dialog.closePromise.then (response) ->
      if response.value and $scope.deleteShiftId > -1
        CalendarService.removeShiftEventsByShiftId shiftId
        CalendarService.removeShift shiftId

    return

  editCalendar = (calendarId) ->
    $scope.editCalendarId = -1
    calendar = CalendarService.getCalendar(calendarId)
    if calendar?
      $scope.editCalendarName = calendar.name
      $scope.editCalendarId = calendar.id

    dialog = ngDialog.open
      template: './app/gestion/editCalendarDialog.html',
      className: 'ngdialog-theme-default'
      scope: $scope
    dialog.closePromise
      .then (response) ->
        if response.value and (not response.value.startsWith '$') and $scope.editCalendarId > -1
          calendar.name = response.value

    return

  editShift = (shiftId) ->
    $scope.editShiftId = -1
    shift = CalendarService.getShift(shiftId)
    if shift?
      $scope.editShiftName = shift.name
      $scope.editShiftId = shift.id
      $scope.editShiftColor = shift.color

    dialog = ngDialog.open
      template: './app/gestion/editShiftDialog.html',
      className: 'ngdialog-theme-default'
      scope: $scope
    dialog.closePromise
      .then (response) ->
        if response.value.editShiftName and $scope.editShiftId > -1
          #Update shift
          shift.name = response.value.editShiftName
          shift.color = response.value.editShiftColor
          #Update Shift Events
          shiftEvents = CalendarService.getShiftEvents()
          _.each shiftEvents, (shiftEvent) ->
            if shiftEvent.shiftId is shiftId
              shiftEvent.backgroundColor = shift.color

    return

  initGrids = ->
    vm.calendarGridOptions.data = CalendarService.getCalendars()
    vm.shiftGridOptions.data=CalendarService.getShifts()
    return

  vm.editCalendar = editCalendar
  vm.editShift = editShift
  vm.deleteCalendar = deleteCalendar
  vm.deleteShift = deleteShift


  initGrids()

  return

GestionWidgetController.$inject = ['CalendarService','ngDialog','lodash','$scope']

angular.module 'calendar'
  .controller 'GestionWidgetController', GestionWidgetController

