BackupWidgetController = (CalendarService,ngDialog) ->
  'ngInject'

  showDialog = (cad) ->
    ngDialog.open({
      template: cad,
      plain: true
    })

  backup = ->
    try
      jsonObj =
        calendars: CalendarService.getCalendars()
        shifts: CalendarService.getShifts()
        events: CalendarService.getEvents()
        shiftEvents: CalendarService.getShiftEvents()

      link = document.createElement("a")
      vm.backupFilename += '.json' if not vm.backupFilename.endsWith '.json'
      link.download = vm.backupFilename
      data = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(jsonObj))
      link.href = "data:" + data
      link.click()
      vm.showDialog '<p>Backup realizado correctamente</p>'
    catch error
      cad = '<p>Error al realizar el backup.</p><br><p>Error: ' + JSON.stringify error
      cad += '</p>'
      vm.showDialog(cad)
      return
    return

  vm = this
  vm.backup = backup
  vm.showDialog = showDialog
  vm.backupFilename = 'Backup_' + new Date().toLocaleDateString().replace(/\//g,'-') + '.json'
  return

BackupWidgetController.$inject = ['CalendarService','ngDialog']

angular.module 'calendar'
  .controller 'BackupWidgetController', BackupWidgetController

