linkFnct = ($scope, element, attrs) ->
  console.log "Creating backupWidget "
  return

backupWidgetFunction = ->
  obj = {
    restrict: 'E',
    link: linkFnct,
    controller:'BackupWidgetController',
    controllerAs:'BackupWidgetCtrl',
    scope: {
    },
    templateUrl:'./app/backup/backupWidget.html'
  }
  return obj

angular.module 'calendar'
  .directive 'backupWidget', backupWidgetFunction
