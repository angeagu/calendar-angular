LodashService = ($window) ->
  return $window._

LodashService.$inject = ['$window']

angular.module 'calendar'
  .factory "lodash", LodashService
