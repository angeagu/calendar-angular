ExportService = (CalendarService) ->

  exportJSON = (calendarSelected,exportFilename) ->
    jsonObj =
      calendars: [calendarSelected]
      shifts: CalendarService.getShifts(calendarSelected.id)
      events: CalendarService.getCalendarEvents(calendarSelected.id)
      shiftEvents: CalendarService.getCalendarShiftEvents(calendarSelected.id)
    link = document.createElement("a")
    exportFilename += '.json' if not exportFilename.endsWith '.json'
    link.download = exportFilename
    data = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(jsonObj))
    link.href = "data:" + data
    link.click()
    return

  exportPDF = () ->
    #PEnding
    return

  #
  # The API
  #
  factory =
    exportJSON: exportJSON,
    exportPDF: exportPDF

  return factory

ExportService.$inject = ['CalendarService']

angular.module "calendar"
  .factory "ExportService",ExportService
