ExportWidgetController = (CalendarService,ExportService,ngDialog) ->
  'ngInject'

  showDialog = (cad) ->
    ngDialog.open({
      template: cad,
      plain: true
    })

  exportCalendar = ->
    calendarSelected = vm.calendars[vm.calendarSelectedId-1]
    formatSelected = vm.exportOptions[vm.formatSelectedId]
    try
      switch formatSelected.format
        when 'json' then ExportService.exportJSON(calendarSelected,vm.exportFilename)
        when 'pdf' then ExportService.exportPDF(calendarSelected,vm.exportFilename)
      vm.showDialog '<p>Calendario guardado correctamente</p>'
    catch error
      cad = '<p>Error al guardar el calendariobackup </p><br><p>Error: ' + JSON.stringify error
      cad += '</p>'
      vm.showDialog(cad)
      return

    return

  changeExportFilename = ->
    vm.exportFilename = vm.calendars[vm.calendarSelectedId-1].name + '.' + vm.exportOptions[vm.formatSelectedId].format
    return

  vm = this
  vm.exportCalendar = exportCalendar
  vm.showDialog = showDialog
  vm.changeExportFilename = changeExportFilename
  vm.calendars = CalendarService.getCalendars()
  vm.exportOptions = [
      id: 0,
      label: 'JSON',
      format: 'json'
    ,
      id: 1,
      label: 'PDF',
      format: 'pdf'

  ]
  vm.calendarSelectedId = vm.calendars[0].id
  vm.formatSelectedId = vm.exportOptions[0].id
  vm.exportFilename = vm.calendars[0].name + '.' + vm.exportOptions[vm.formatSelectedId].format


  return

ExportWidgetController.$inject = ['CalendarService','ExportService','ngDialog']

angular.module 'calendar'
  .controller 'ExportWidgetController', ExportWidgetController

