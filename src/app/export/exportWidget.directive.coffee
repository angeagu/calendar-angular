linkFnct = ($scope, element, attrs) ->

exportWidgetFunction = ->
  obj = {
    restrict: 'E',
    link: linkFnct,
    controller: 'ExportWidgetController',
    controllerAs: 'ExportWidgetCtrl',
    scope: {
    },
    templateUrl:'./app/export/exportWidget.html'
  }
  return obj

angular.module 'calendar'
  .directive 'exportWidget', exportWidgetFunction
